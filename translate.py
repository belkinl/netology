import requests
import os

def translate_it(input_file, output_file, lang_from, lang_to='ru'):
    """
    YANDEX translation plugin
    docs: https://tech.yandex.ru/translate/doc/dg/reference/translate-docpage/
    https://translate.yandex.net/api/v1.5/tr.json/translate ?
    key=<API-ключ>
     & text=<переводимый текст>
     & lang=<направление перевода>
     & [format=<формат текста>]
     & [options=<опции перевода>]
     & [callback=<имя callback-функции>]
    :param text: <str> text for translation.
    :return: <str> translated text.
    """
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
    key = 'trnsl.1.1.20180920T194823Z.c163b31990782a44.990476362e0a6339b2f3b6914e61dee2febcfa61'

    params['key'] = key
    with open(input_file) as f:
        params['text'] = f.read()
    params['lang'] = lang_from + '-' + lang_to
    response = requests.get(url, params=params).json()
    with open(output_file, 'w', encoding='utf-8') as f:
        f.write(' '.join(response.get('text', [])))


news = ['DE.txt', 'ES.txt', 'FR.txt']
for file in news:
    lang_from = file.strip('.txt').lower()
    lang_to = 'ru'
    out_file = lang_to.upper() + '_' + file
    input_path = os.path.abspath(file)
    output_path = os.path.join(os.path.dirname(input_path), out_file)
    translate_it(input_path, output_path, lang_from)
