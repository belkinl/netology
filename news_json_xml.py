import json
import xml.etree.ElementTree as ET


def sort_by_count(word_list):
    word_count = dict()
    for word in word_list:
        if len(word) > 6:
            word_count[word.lower()] = word_list.count(word)

    return sorted(word_count.items(), key=lambda count: count[1], reverse=True)


news_words_xml = list()
news_words_json = list()

tree = ET.parse('files/newsafr.xml')

for description in tree.findall('channel/item/description'):
    news_words_xml += description.text.split(' ')

with open('files/newsafr.json') as df_json:
    news = json.load(df_json)
    for description in news['rss']['channel']['items']:
        news_words_json += description['description'].split(' ')

top_list_xml = sort_by_count(news_words_xml)
top_list_json = sort_by_count(news_words_json)

print('rank  xml   json')
for i in range(10):
    print(f'{i+1}  {top_list_xml[i][0]} ({top_list_xml[i][1]})   {top_list_json[i][0]} ({top_list_json[i][1]})')
