class Animal:
    voice = None
    animal_list = list()

    def __init__(self, name, weight):
        self.name = name
        self.weight = weight
        self.animal_list.append((self.__class__.__name__.lower(), self.weight))

    def feed(self):
        print(f'The {self.__class__.__name__.lower()} {self.name} was fed \n')

    def call(self):
        print(f'{self.__class__.__name__} says {self.voice}-{self.voice}')


class Bird(Animal):

    def collect_egg(self):
        print(f'Eggs by the {self.__class__.__name__.lower()} {self.name} was picked \n')


class Ungulate(Animal):

    def collect_milk(self):
        print(f'The {self.__class__.__name__.lower()} {self.name} was milked \n')


class Sheep(Ungulate):
    voice = 'baa'

    def trim(self):
        print(f'The sheep {self.name} trimmed \n')


class Cow(Ungulate):
    voice = 'moo'


class Goat(Ungulate):
    voice = 'naa'


class Duck(Bird):
    voice = 'quack'


class Chicken(Bird):
    voice = 'cluck'


class Goose(Bird):
    voice = 'honk'


animals_list = Animal.animal_list

goose_1 = Goose('Seriy', 5)
goose_2 = Goose('Beliy', 4.5)

cow_1 = Cow('Manka', 150)

sheep_1 = Sheep('Barashek', 80)
sheep_2 = Sheep('Kudryaviy', 90)

chicken_1 = Chicken('Ko-Ko', 2)
chicken_2 = Chicken('Kukareku', 1.5)

goat_1 = Goat('Roga', 18)
goat_2 = Goat('Kopyta', 21)

duck_1 = Duck('Kryakva', 3)

# Interaction examples

duck_1.call()
cow_1.collect_milk()
sheep_2.trim()
goose_2.collect_egg()

max_weight = max([i[1] for i in animals_list])
for i in animals_list:
    if i[1] == max_weight:
        print(f'The {i[0]} has weight {max_weight} kg. It is the maximum!!\n')

print(f'Summary weight is {sum([i[1] for i in animals_list])} kg\n')