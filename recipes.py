from pprint import pprint


def add_ing_to_meal(ing):
    recipe = dict()
    ing_list = ing.strip().split(' | ')
    recipe['ingredient_name'] = ing_list[0]
    recipe['quantity'] = int(ing_list[1])
    recipe['measure'] = ing_list[2]
    return recipe


def get_shop_list_by_dishes(cook_book, dishes_list, cnt):
    shop_list = dict()
    for dish in dishes_list:
        dish = dish.strip().capitalize()
        if dish in cook_book:
            for ingredient in cook_book[dish]:
                if ingredient['ingredient_name'] in shop_list.keys():
                    shop_list[ingredient['ingredient_name']]['quantity'] += ingredient['quantity'] * cnt
                else:
                    shop_list[ingredient['ingredient_name']] = {'measure': ingredient['measure'], 'quantity': ingredient['quantity'] * cnt}
    return shop_list


def create_cook_book():
    recipes_dict = dict()
    with open('recipes_raw.txt', 'r') as f:
        for meal in f:
            name = meal.strip()
            count_ing = int(f.readline())
            for i in range(count_ing):
                ing = f.readline()
                if name in recipes_dict:
                    recipes_dict[name].append(add_ing_to_meal(ing))
                else:
                    recipes_dict[name] = [add_ing_to_meal(ing)]
            f.readline()
    return recipes_dict


cook_book = create_cook_book()
pprint(cook_book)
while True:
    person_count = int(input('Введите количество персон: '))
    dishes = input(f'Введите названия блюд из списка через запятую {cook_book.keys()}: ').split(',')
    print()
    pprint(get_shop_list_by_dishes(cook_book, dishes, person_count))
    if input('Повторить? [y/n]').lower() in ['y', 'н', 'yes', 'да', '1']:
        continue
    else:
        break
